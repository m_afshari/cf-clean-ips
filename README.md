# CF Clean Ip

## Installation

```shell
pip install -r requirements.txt
python src/collect.py
```

## Providers
- [ircf.space](ircf.space)
- [amiajoketoyou.lol](amiajoketoyou.lol)
- [eqjgkx.xyz](eqjgkx.xyz)
- [adihnrr.eu.org](adihnrr.eu.org)
- [fastynet.cloud](fastynet.cloud)
- [chamburak520.online](chamburak520.online)
- [vcdn.online](vcdn.online)
- [ma7ta.online](ma7ta.online)
- [nazario.top](nazario.top)
- [boby66600.eu.org](boby66600.eu.org)
- [cjmahdi.online](cjmahdi.online)
- [r-acc3ss.cloud](r-acc3ss.cloud)
- [hamedgeek.top](hamedgeek.top)
- [jangal.icu](jangal.icu)
- [nvsnm.com](nvsnm.com)
- [kojachera.cf](kojachera.cf)
- [myips.us.eu.org](myips.us.eu.org)
- [filna.cloud](filna.cloud)
- [ipcf.cloud](ipcf.cloud)

## Resources 
- Forked from the project [cf-clean-ips](https://github.com/vfarid/cf-clean-ips/)
